const express = require("express");
const router = express.Router(); //connect to express
const userControllers = require("../controllers/userControllers.js");
const courseControllers = require("../controllers/courseControllers.js");
const auth = require("../auth");

// Check email if it is existing to our database
router.post("/checkEmail", (req,res) =>{
	userControllers.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

// User Registration route
router.post("/register", (req,res) => {
	userControllers.registerUser(req.body).then(resultFromController => res.send(resultFromController));

})

// User Login
router.post("/login", (req,res) => {
	userControllers.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Retrieving user details
//  auth.verify - middleware to ensure that the user is loged in before they can retrieve their details
router.post("/details", auth.verify, (req, res) => {
	
	// uses the decode method defined in the auth.js to retrieve user info from request header
	const userData = auth.decode(req.headers.authorization)

	userControllers.getProfile({userId: req.body.id}).then(resultFromController => res.send(resultFromController));
})


// Enroll a user

router.post("/enroll", auth.verify, (req, res) => {

	//value needed to enroll, one for the user who will enroll, one for the course
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	}

	userControllers.enroll(data).then(resultFromController => res.send(resultFromController))
})

module.exports = router;// use to allow page to acces routes/user.js file// this is to connect the route to other modules