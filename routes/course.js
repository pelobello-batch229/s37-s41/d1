const express = require("express");
const router = express.Router();
const courseControllers = require("../controllers/courseControllers.js");
const auth = require("../auth");

// Create course
router.post("/", auth.verify, (req, res) => {

    const data = {
        course: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }

    courseControllers.addCourse(data).then(resultFromController => res.send(resultFromController));

});



//
/* Create course
router.post("/", (req, res) => {
    courseControllers.addCourse(req.body).then(resultFromController => res.send(resultFromController))
})

// export the router object for index.js file
module.exports = router;
*/


router.get("/all", (req, res) => {
    courseControllers.getAllCourses().then(resultFromController => {
        res.send(resultFromController)
    });
})


/*
    Mini-Acitivity:
    1. Create a route that will retrieve ALL ACTIVE courses
    2. No need for user to login
    3. Create a controller that will return all active coourses
    4. Send your postman output screenshot in our batch

*/

router.get("/active", (req ,res) => {
   courseControllers.getActiveCourses(req.body).then(resultFromController => res.send(resultFromController));


})

// retrieve a specific course
router.get("/:courseId", (req, res) => {
    courseControllers.getCourse(req.params).then(resultFromController => res.send(resultFromController));
})


// update a course
router.put("/:courseId", auth.verify, (req,res) => {
    courseControllers.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
})
// export the router object for index.js file
module.exports = router;


// Activity pt3
// Archiving a Course
router.put("/:courseId/archive", auth.verify, (req,res) => {

      const data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    courseControllers.archiveCourse(req.params, req.body, data).then(resultFromController => res.send(resultFromController));
})
