const User = require("../models/User.js");
const Course = require("../models/Course.js");
const bcrypt = require("bcrypt");// to use the npm i bcrypt
const auth = require("../auth.js")
// Check if the email already exists
/*
Steps: 
1. Use mongoose "find" method to find duplicate emails
2. Use the "then" method to send a response back to the frontend appliction based on the result of the "find" method
*/

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0 ){
			return true;
		}else{
			return false;
		}
	})
}

// User registration
/*
Steps:
1. Create a new User object using the mongoose model and the information from the request body
2. Make sure that the password is encrypted
3. Save the new User to the database
*/

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		// 10 is the value provided as the number as "salt"
		password: bcrypt.hashSync(reqBody.password, 10),
		isAdmin: reqBody.isAdmin
	})
	// Saves the created objecto to our database
	return newUser.save().then((user,error) => {
		// Registration failed
		if(error){
			return false;	
		}else{
			// if registration is successfull
			return true;//change to user to view the input user
		}
	})
}

// User authentication
/*
Steps:
1. Check the database if the user email exists
2. Compare the password provided in the login form with the password stored in the database
3. Generate/return a JSON web token if the user is successfully logged in and return false if not
*/

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return false;
		}else{
			// i cocompare ni database yung pass na nilgay sa postman
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if(isPasswordCorrect){//truthy
				//default syntax for creating token for the user
				return {access: auth.createAccessToken(result)}

			}else{
				return false;
			}
		}
	})
}

// Get Profile
/*
	Steps:
	1. Find the document in the database using the User's ID
	2. Reassign the password of the returned documents to an empty string
	3. Return the result back to the frontend
*/

module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {
		console.log(result);

		result.password = "";

		return result;
	});
}

//  Enroll

/*
	Steps:
	1. Find the document in the database using the user's id
	2. Add the course id to the user's enrollment array
	3. Update the document
*/
// Async awit - will be used in enrolling the user
// this will need to update 2 seperate documents
module.exports.enroll = async (data) => {

	let userAdmin= await User.findById(data.userId).then(isadmin => {return isadmin.isAdmin});
	
	if(data.userId && userAdmin === true){
			// create isUserUpdated variable that will return true upon successful update
			// await keyword - will allow the enroll method to complete updating the user before returning a response back to he frontend
		let isUserUpdated = await User.findById(data.userId).then(user => {
			console.log(user.isAdmin);
			// adds the courId to the user's enrollments array
			// push - adds the courseId in your existing documents
			user.enrollments.push({courseId: data.courseId})

			//always 1st and object na marereceive then yung error// bawal pag baliktarin
			//save() is method na nag sesesave sa database
			return user.save().then((user, error) => {

				if(error){
					return false;
				} else {
					return true;
				}
			})
		})

		// adds the user id in the enrollees array of the course
		// await - to allow the enroll method to complete updating the course before
		// returning a response to the frontend

		let isCourseUpdated = await Course.findById(data.courseId).then(course => {

			// add the user id in course's enrolless array
			console.log(course)
			course.enrollees.push({userId: data.userId});

			//  saves the updated course info in the database
			return course.save().then((course, error) => {
				// if failed
				if(error){
						return false;
				// if successful
				} else {
					return true;
				}
			})	
		})

		if(isUserUpdated && isCourseUpdated){// truthy
			// successful
			return true;
		} else {
			// failed
			return false;
		}
	} else {
		return ("Unauthorezed User Pls Login/Register")
	}



}

// visual user.enrollements
/*
userId: user1
courseId: course1

userId: {
	fname;
	lname
	email
	pw;
	isActive
	contact
	enrolments: [courseId]

}

let isUserUpdateed = true;

*/

/*
// Visusal course.enrollees

course1 ={
	name
	price
	description
	isActive
	enrolless:
}

let isCourseUpdated = true
*/
/*

isUserUpdated && isCourseUpdated
true && true
return true(successfully enrolled)
*/