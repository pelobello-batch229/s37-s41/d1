const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/user.js");
const courseRoutes = require("./routes/course.js");

const app = express();

// MongODB Connection
mongoose.connect("mongodb+srv://admin:admin1234@cluster0.mr7hck1.mongodb.net/s37-s41?retryWrites=true&w=majority",{
	useNewUrlParser : true,
	useUnifiedTopology: true,
});

let db = mongoose.connection; // server para kay mongodb

db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the cloud database"));

// MiddleWares\
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use("/users", userRoutes);
app.use ("/courses", courseRoutes);

	// Para pag magdedeployed na sa internet
app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${process.env.PORT || 4000}`)
});

